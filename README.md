Test the API with a call like this
https://ldme8v45d9.execute-api.us-east-1.amazonaws.com/env1/transactions?transactionId=5&type=clothing&amount=234.00

Tutorial video https://youtu.be/uFsaiEhr1zs

## Changes and improvement in code from manual API deployment to Terraforming are below.

### Might see the commits as similiar to the steps mentioned below so the project transformation is understood.


1. Create Lambda, API Gateway as per above tutorial and test it.
	- Result: Passed in the first attempt
2. Create Lambda code and service through terraform and connect and test through same manual API Gateway above.
	- Result: Passed in the first attempt but we can't see the CloudWatch logs.
3. Create the permission polcies to CreateLogGroup, CreateLogStream, PutLogEvents in and use the resource arn of lambda name exactly for the log group name.
    - Result: Lambda starts to work with hit from API Gateway URL but we have to redeploy the API everytime terraform is run.
4. Change the policy documents location to separate json files.
    - Result: Same as above.
5. Change the policy documents location to separate json files in a separate folder, use templatefile function.
    - Result: Same as above.
6. Moved the long paths to variables in the root module.
    - Result: Same as above.
7. Created apigateway module with below resources
    - aws_api_gateway_rest_api
    - aws_api_gateway_resource
    - aws_api_gateway_method
    - aws_api_gateway_method_response
    - aws_api_gateway_integration
    - aws_api_gateway_integration_response
    - aws_lambda_permission
    - Passing the below parameters from the lambda module
    - aws_lambda_function.lambda_function.function_name.
    - aws_lambda_function.lambda_function.invoke_arn
    - Result: All the resources are created properly. But getting {"message":"Forbidden"} response when hit through URL in the browser.
    Lambda function is working properly, getting proper response and getting Cloudwatch log for it.
8. Went to API Gateway --> Resources in AWS Console UI and did the Deploy API, created a new environment env1.
    - Result: Everything worked. It looks like we have to find a way to deploy API through terraform.
9. Created the following deployment related resources in terraform
    - aws_api_gateway_deployment
    - aws_api_gateway_stage
    - aws_api_gateway_method_settings
    - Result: this lead in the following error in the terraform apply: 
    │ Error: updating API Gateway Stage failed: BadRequestException: CloudWatch Logs role ARN must be set in account settings to enable logging.
10. Made changes to create below
    - aws_iam_role_policy: to take a custom role as per terraform official documentation
    - aws_api_gateway_account: to accept the role arn of assume role policy
    - removed aws_iam_role_policy_attachment
    - Result: terraform apply passed with no errors. URL and parameters are working as expected.
11. Reusing the custom policy instead of using the standard log permissions as per the terraform official portal in aws_iam_role_policy resource. https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account
    - Result: working as expected. getting the result properly.
12. Used Standalone policy using aws_iam_role_policy_attachment instead of inline policy aws_iam_role_policy
    - Result: working as expected. getting the result properly.
13. Tested and updated the documentation for 2 kinds of role policy attachments below.
    - aws_iam_role_policy: We don't see a role in Policy usage section for arn:aws:iam::480985514154:policy/api_gateway_logs_policy
    - aws_iam_role_policy_attachment: We see a assume_role_for_api_gateway role in Policy usage section for arn:aws:iam::480985514154:policy/api_gateway_logs_policy
    - Result: working as expected. getting the result properly.