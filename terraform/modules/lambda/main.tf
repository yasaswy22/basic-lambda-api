    data "archive_file" "lambda_zipper" {
    type = "zip"
    source_file = "${var.lambda_function_location}/${var.lambda_file_name}.py"
    output_path = "${var.lambda_function_location}/${var.lambda_file_name}.zip"
}


data "aws_iam_policy_document" "policy_doc_logs" {
    source_policy_documents   = [var.lambda_cloudwatch_log_permissions_policy]
}

data "aws_iam_policy_document" "policy_doc_assume_role" {
    source_policy_documents   = [var.lambda_assume_role_policy]
}

resource "aws_iam_policy" "lamda_cloudwatch_logs_permission_policy" {
  name   = "lambda_logs_policy"
  path   = "/"
  policy =  data.aws_iam_policy_document.policy_doc_logs.json
}

resource "aws_iam_role" "assume_role_lambda" {
    name               = "assume_role_for_lambda"
    assume_role_policy = data.aws_iam_policy_document.policy_doc_assume_role.json
}

resource "aws_iam_role_policy_attachment" "policy_role_attached" {
    role       = aws_iam_role.assume_role_lambda.name
    policy_arn = aws_iam_policy.lamda_cloudwatch_logs_permission_policy.arn
}

resource "aws_lambda_function" "lambda_function" {
    filename = "${var.lambda_function_location}/${var.lambda_file_name}.zip"
    function_name = var.lambda_file_name
    handler = "${var.lambda_file_name}.lambda_handler"
    role = aws_iam_role.assume_role_lambda.arn
    source_code_hash = data.archive_file.lambda_zipper.output_base64sha256
    runtime = var.selected_runtime_env
    timeout = var.timeout

    tags = var.tags
}