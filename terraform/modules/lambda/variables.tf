variable "lambda_function_location"{
}

variable "lambda_assume_role_policy"{
}

variable "lambda_cloudwatch_log_permissions_policy" {
  
}

variable "lambda_file_name"{
}

# variable "lambda_role_arn" {
# }

variable "selected_runtime_env" {
}

variable "timeout" {
}

variable "tags" {
    type = map(string)
}