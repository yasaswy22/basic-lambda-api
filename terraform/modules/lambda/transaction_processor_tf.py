import json
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event,context):
    try:
        # Retrieve data from event body.
        json_dictionary = json.dumps(event)
        logging.info(f'Printing the event passed into Lambda by API Gatway: {json_dictionary}')

        # 1. Parse out the query string params
        transactionID = event['queryStringParameters']['transactionId']
        transactionType = event['queryStringParameters']['type']
        transactionAmount = event['queryStringParameters']['amount']
        
        logging.info(f'transactionID is {transactionID}')
        logging.info(f'transactionType is {transactionType}')
        logging.info(f'transactionAmount is {transactionAmount}')
                

        # 2. Construct the body of the response object
        transactionResponse = {}
        transactionResponse['transactionId'] = transactionID
        transactionResponse['type'] = transactionType
        transactionResponse['amount'] = transactionAmount
        transactionResponse['message'] = "Constructed response body in Lambda through Terraform"

        # 3. Construct the http response object
        responseObject = {}
        responseObject['statusCode'] = 200
        responseObject['headers'] = {}
        responseObject['headers']['Content-Type'] = 'application/json'
        responseObject['body'] = json.dumps(transactionResponse)

        logging.info(f'Logging the constructed response object formmed within the Lambda: {responseObject}')
        return responseObject

    except Exception as e:
        logging.error("Unable to form the response object in Lambda", exc_info=True)
        return f' Error occured: {e}'
    
    # Test the API with a call like this
    # https://ldme8v45d9.execute-api.us-east-1.amazonaws.com/env1/transactions?transactionId=5&type=clothing&amount=234.00
    # Tutorial video https://youtu.be/uFsaiEhr1zs