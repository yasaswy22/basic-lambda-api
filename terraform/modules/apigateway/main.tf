# API Gateway creation starts
resource "aws_api_gateway_rest_api" "transactions_api_gateway" {
    name = "transactions_api_tf"
    description = "Created through terraform"
    endpoint_configuration {
        types = ["REGIONAL"]
    }
    
    tags = var.tags
}

# *
# *
# Integration Section
# *
# *

# Create transaction integration section starts
resource "aws_api_gateway_resource" "transactions_resource" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    parent_id = aws_api_gateway_rest_api.transactions_api_gateway.root_resource_id
    path_part = "transactions"
}

# GET method
resource "aws_api_gateway_method" "transactions_get_method" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    resource_id = aws_api_gateway_resource.transactions_resource.id
    http_method = "GET"
    authorization = "NONE"

    # MANUAL UI VIEW:
    # Method Request
    # ----------------
    # Auth: NONE
    # ARN: arn:aws:execute-api:us-east-1:480985514154:h3m66k5yxe/*/GET/transactions
}

# GET method response
resource "aws_api_gateway_method_response" "transactions_get_method_response" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    resource_id = aws_api_gateway_resource.transactions_resource.id
    http_method = aws_api_gateway_method.transactions_get_method.http_method
    status_code = "200"
    response_models = {
        "application/json" = "Empty"
    }
    response_parameters = {
        "method.response.header.Access-Control-Allow-Headers" = true,
        "method.response.header.Access-Control-Allow-Methods" = true,
        "method.response.header.Access-Control-Allow-Origin" = true
    }

    # add an explicit depends_on for clean runs
    depends_on = [aws_api_gateway_method.transactions_get_method]

    # MANUAL UI VIEW:
    # Method Response
    # -----------------
    # HTTP Status: Proxy
    # Models: application/json => Empty
}

# GET method integration
resource "aws_api_gateway_integration" "transactions_get_integration" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    resource_id = aws_api_gateway_resource.transactions_resource.id
    http_method = aws_api_gateway_method.transactions_get_method.http_method
    
    # integration_http_method specifies how API Gateway will interact with the back end
    # Required if type is AWS, AWS_PROXY, HTTP, HTTP_PROXY
    # Lambda function can only be invoked via POST
    integration_http_method = "POST"
    type = "AWS_PROXY"
    uri = var.lambda_arn

    # MANUAL UI VIEW:
    # Integration Request
    # --------------------
    # Type: LAMBDA_PROXY
}

# GET method integration response
resource "aws_api_gateway_integration_response" "transactions_get_integration_response" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    resource_id = aws_api_gateway_resource.transactions_resource.id
    http_method = aws_api_gateway_method.transactions_get_method.http_method
    status_code = "200"
    response_parameters = {
        "method.response.header.Access-Control-Allow-Origin" = "'*'"
    }
    depends_on = [aws_api_gateway_integration.transactions_get_integration]

    # MANUAL UI VIEW: 
    # Integration Response
    # -------------------
    # Proxy integrations cannot be configured to transform responses.
}

# GET method lambda permission
resource "aws_lambda_permission" "transactions_get_lambda_permission" {
    statement_id  = "AllowExecutionFromAPIGateway"
    action        = "lambda:InvokeFunction"
    function_name = var.lambda_function_name
    principal     = "apigateway.amazonaws.com"

    # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
    # Control who can call an API Gateway API method with IAM policies
    
    # MANUAL UI VIEW: It is shown as below pop-up when we create it manually.
    # You are about to give API Gateway permission to invoke your Lambda function:
    # arn:aws:lambda:us-east-1:480985514154:function:TrasactionProcessor
    # it shows the below ARN in the TEST screen
    # arn:aws:execute-api:us-east-1:480985514154:h3m66k5yxe/*/GET/transactions
    source_arn = "arn:aws:execute-api:us-east-1:480985514154:${aws_api_gateway_rest_api.transactions_api_gateway.id}/*/${aws_api_gateway_method.transactions_get_method.http_method}${aws_api_gateway_resource.transactions_resource.path}"
}

# *
# *
# Integration Section ends here
# *
# *

# API Gateway deployment section begins
resource "aws_api_gateway_deployment" "api_gateway_deploy_api" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    
    # variables - (Optional) Map to set on the stage managed by the stage_name argument.
    variables = {
      "deployed_at" = "${timestamp()}"
    }

    # Enable the resource lifecycle configuration block create_before_destroy argument in this resource configuration to properly order redeployments in Terraform. Without enabling create_before_destroy, API Gateway can return errors such as BadRequestException: Active stages pointing to this deployment must be moved or deleted on recreation.
    lifecycle {
      create_before_destroy = true
    }
    depends_on = [aws_lambda_permission.transactions_get_lambda_permission]
}

# Create a stage environment for the deployment of API
resource "aws_api_gateway_stage" "api_gateway_stage" {
    deployment_id = aws_api_gateway_deployment.api_gateway_deploy_api.id
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    stage_name = "env1"
    depends_on = [aws_api_gateway_deployment.api_gateway_deploy_api]
}

# aws_api_gateway_method_settings manages API Gateway Stage Method Settings. For example, CloudWatch logging and metrics.
# metrics_enabled - (Optional) Whether Amazon CloudWatch metrics are enabled for this method.
# logging_level - (Optional) Logging level for this method, which effects the log entries pushed to Amazon CloudWatch Logs. The available levels are OFF, ERROR, and INFO.

resource "aws_api_gateway_method_settings" "all" {
    rest_api_id = aws_api_gateway_rest_api.transactions_api_gateway.id
    stage_name  = aws_api_gateway_stage.api_gateway_stage.stage_name
    method_path = "*/*"

    settings {
        metrics_enabled = true
        logging_level   = "ERROR"
    }
    depends_on = [aws_api_gateway_stage.api_gateway_stage]
}

# We get the following error when Cloudwatch arn is not set to API Gateway account.
# Error: updating API Gateway Stage failed: BadRequestException: CloudWatch Logs role ARN must be set in account settings to enable logging
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/api_gateway_account
# Below is the code to fix that error

data "aws_iam_policy_document" "policy_doc_logs" {
    source_policy_documents   = [var.api_gateway_cloudwatch_log_permissions_policy]
}

data "aws_iam_policy_document" "policy_doc_assume_role" {
    source_policy_documents   = [var.api_gateway_assume_role_policy]
}

output "printing_api_gateway_cloudwatch_logs_permission_policy" {
    value = data.aws_iam_policy_document.policy_doc_logs.json
}

output "printing_api_gateway_assume_role_policy" {
    value = data.aws_iam_policy_document.policy_doc_assume_role.json
}

resource "aws_iam_policy" "api_gateway_cloudwatch_logs_permission_policy" {
  name   = "api_gateway_logs_policy"
  path   = "/"
  policy =  data.aws_iam_policy_document.policy_doc_logs.json
}

resource "aws_iam_role" "assume_role_api_gateway" {
    name = "assume_role_for_api_gateway"
    assume_role_policy = data.aws_iam_policy_document.policy_doc_assume_role.json
}

#Standalone policy attachment *read more information below.
resource "aws_iam_role_policy_attachment" "api_gateway_cloudwatch_logs_permission_attached" {
    role       = aws_iam_role.assume_role_api_gateway.name
    policy_arn = aws_iam_policy.api_gateway_cloudwatch_logs_permission_policy.arn
}

#Inline policy attachment *read more information below.
# resource "aws_iam_role_policy" "api_gateway_cloudwatch_logs_permission_attached" {
#     name = "cloudwatch_logs_permission_tf"
#     role       = aws_iam_role.assume_role_api_gateway.id
#     policy = data.aws_iam_policy_document.policy_doc_logs.json
# }

resource "aws_api_gateway_account" "account_settings_enable_logging" {
  cloudwatch_role_arn = aws_iam_role.assume_role_api_gateway.arn
}

# API Gateway deployment section ends


# * more information on policy attachment
# aws_iam_role - This creates a role as would be the case if you would click on the Role link.

# aws_iam_policy - This creates a "standalone" policy, I.E. what you would create if you click on the "Policy" link in IAM. This policy can be attached to multiple roles.

# aws_iam_role_policy - This creates an inline policy which would be created if you navigated to an existing Role, and clicked on "Add inline policy". This policy is associated with a single role only. We don't see a role in Policy usage section for arn:aws:iam::480985514154:policy/api_gateway_logs_policy

# aws_iam_role_policy_attachment - This will allow you to attach a "standalone" policy (I.E. one created with aws_iam_policy) to an IAM role (I.E. one created by aws_iam_role). We see a assume_role_for_api_gateway role in Policy usage section for arn:aws:iam::480985514154:policy/api_gateway_logs_policy

# aws_iam_policy_attachment - I personally haven't had to use this yet, but it is similar to aws_iam_role_policy_attachment. Per the [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy_attachment.html), it creates an exclusive attachment meaning that the referenced policy can only be attached to a single role across your entire AWS account.

# Use case: Say you have multiple roles that need the same access to say an S3 bucket. You can create that same policy inline for each role. Alternatively, you can create a stand alone policy and attach it to each of the roles. The second option means if you need to change the permissions across the board, just change the standalone policy and all roles get the update.