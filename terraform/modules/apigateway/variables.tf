variable "tags" {
  
}

variable "lambda_arn" {
  
}

variable "lambda_function_name" {
  
}

variable "api_gateway_assume_role_policy" {
  
}

variable "api_gateway_cloudwatch_log_permissions_policy" {
  
}