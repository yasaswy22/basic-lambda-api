module "lambda" {
    source = "./modules/lambda"
    lambda_function_location = "${path.module}/${var.lambda_functions_path}"
    lambda_assume_role_policy = templatefile("${path.module}/${var.assume_role_policy_path}", 
                                                {service_name  = "lambda"})
    lambda_cloudwatch_log_permissions_policy = templatefile("${path.module}/${var.log_permissions_policy_path}", 
                                                {log_group_name  = "transaction_processor_tf"})

    lambda_file_name = "transaction_processor_tf"
    selected_runtime_env = var.runtime_main["python38"]
    timeout = var.lambda_timeout_main
    tags = var.global_tags
}

module "apigateway" {
    source = "./modules/apigateway"
    api_gateway_assume_role_policy = templatefile("${path.module}/${var.assume_role_policy_path}", 
                                                {service_name  = "apigateway"})
    api_gateway_cloudwatch_log_permissions_policy = templatefile("${path.module}/${var.log_permissions_policy_path}", 
                                                {log_group_name  = "transaction_processor_tf"})
    lambda_function_name = module.lambda.lambda_function_name
    lambda_arn = module.lambda.lambda_function_arn
    tags = var.global_tags
}

