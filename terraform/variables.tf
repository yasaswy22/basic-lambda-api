variable "runtime_main" {
  type = map(any)
  default = {
    "python37" = "python3.7"
    "python38" = "python3.8"
    "nodejs12" = "nodejs12.x"
  }
}

variable "lambda_timeout_main" {
  default = "60"
}

variable "global_tags"{
    description = "(Optional) A mapping of tags to assign to the bucket."
    type = map(string)
    default = {
        environment = "test"
        name = "terraform_prac"
    }   
}

variable "assume_role_policy_path" {
    default = "modules/policies/assume_role.json"
}

variable "log_permissions_policy_path" {
    default = "modules/policies/cloudwatch_logs_permission.json"
}

variable "lambda_functions_path" {
    default = "modules/lambda"
}